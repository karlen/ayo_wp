############################################
# Slack Configuration
############################################

# Required
set :slack_subdomain, 'subdomain' # if your Slack subdomain is example.slack.com
set :slack_url, 'https://my.slack.com/services/new/incoming-webhook' # https://my.slack.com/services/new/incoming-webhook

# Optional
#set :slack_channel, '#channel'
#set :slack_emoji, ':rocket:'
