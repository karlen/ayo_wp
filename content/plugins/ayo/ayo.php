<?php
/**
 * Entry point for the plugin.
 *
 * This file is read by WordPress to generate the plugin information in the
 * admin panel.
 *
 * @package WP_Plugin
 *
 * Plugin Name: Ayo
 * Author: Karlen Avetisyan <karlen.avetisyan@astgeek.com>
 * Description: Ayo theme main functionality.
 * Version:     0.0.1
 */
if ( ! defined( 'WPINC' ) ) {
	die;
}
require_once "autoload.php";
add_action( 'admin_init', 'ayo_init' );

function ayo() {

}

add_action( 'admin_menu', 'ayo_plugin_menu' );

function ayo_plugin_menu() {
	$icon = plugin_dir_url( __FILE__ ).'/assets/img/logo_small.png';
	add_menu_page( 'Ayo', 'Ayo', 'manage_options', 'titus', 'ayo_admin_render', $icon, 38 );

}

function ayo_admin_render() {

}

function load_module($moduleName,  $args = false){
	$module = '\Modules\\'.$moduleName;
	$module = new $module;
	$args ? $module->init($args) : $module->init();

}

function get_module($moduleName,  $args = false){
	$module = '\Modules\\'.$moduleName;
	$module = new $module;
	return $args ? $module->init_get($args) : $module->init_get();
}


function load_modules($modules){
    foreach ($modules as $module) {
        if(is_array($module)) {
            load_module($module['name'], $module['args']);
        }else {
            load_module($module);
        }
    }
}