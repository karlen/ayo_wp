<?php
if (!session_id()) {
    session_start();
}
require_once dirname( __FILE__ ) . '/ajax.php';
add_action( 'init', 'ayo_init', 1 );


function ayo_init()
{
    add_filter( 'show_admin_bar', '__return_false' );

    add_image_size( '600x600', 600, 600, true );
    add_image_size( '600x400', 600, 400, true );

    add_theme_support('post-thumbnails');

}

function get_post_image_src( $post_id, $size = 'full' ) {

    $featured_image_src = '';
    $featured_image_id = get_post_thumbnail_id( $post_id );


    $featured_image = wp_get_attachment_image_src( $featured_image_id, $size );

    if ( ! empty( $featured_image ) ) {
        $featured_image_src = $featured_image[0];
    }
    return $featured_image_src;
}


function tags_to_string($ID) {
    $tags = wp_get_object_terms( $ID, 'post_tag');
    $ret = "";
    foreach ($tags as $tag){
        $ret .= ucfirst($tag->name).", ";
    }
    $ret = trim($ret, ', ');
    return $ret;
}


add_filter('acf/fields/google_map/api', function ($api) {
    $api['key'] =  'AIzaSyAbIdx3615VVAqjHnyLlxLtuY0a6zNL9HQ';
    return $api;
});