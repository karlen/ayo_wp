<?php
/**
 * The template for displaying all pages.
 *
 * @package WordPress
 * @subpackage Ayo
 */
get_header();
load_modules([
    'Navigation',
    'Slider',
    'Page',
    'Share',
    'Subscribe',
    'Calendar',
    'Contact',
    'Footer',
]);
get_footer();
