<?php
add_action( 'wp_ajax_nopriv_follow', 'ayo_follow' );
add_action( 'wp_ajax_follow', 'ayo_follow' );

function ayo_follow(){
    $follow = new \Modules\Subscribe();
    $follow->follow();
    exit();
}

add_action( 'wp_ajax_nopriv_contact_us', 'titus_contact_us' );
add_action( 'wp_ajax_contact_us', 'titus_contact_us' );

function titus_contact_us(){
    $follow = new \Modules\Contact();
    $follow->add();
    exit();
}