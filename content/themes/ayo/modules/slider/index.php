<?php
namespace Modules;

use \Ayo\Core\Module;

class Slider extends Module
{
    protected function getSliderData() {
        global $lang;

        if(is_home()){
            $data = get_field('slider', 'option');
            $sliderData = array();
            foreach ($data as $item) {
                if($item['add_from_post']) {
                    $post = pll_get_post($item['post']);
                    if($post) {
                        $sliderData[] = array(
                            "title" => $post->post_title,
                            "subtitle" => $post->post_excerpt,
                            "image" => get_post_image_src($post->ID),
                            "link" => get_permalink($post)
                        );
                    }
                }else {
                    $sliderData[] = array(
                        "title" => $item['title'][0][$lang],
                        "subtitle" => $item['subtitle'][0][$lang],
                        "image" => $item['image']['url'],
                        "link" => $item['link']
                    );
                }
            }
            return $sliderData;
        }

    }
    protected function setData(){
        $this->data = array(
            "slider" => $this->getSliderData()
        );
    }
}