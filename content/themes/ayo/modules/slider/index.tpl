<div data-module="slider">
    <div class="carousel-wrapper">
        <div id="header_carousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                {foreach from=$slider key=k item=item}
                    <li data-target="#header_carousel" {if $k === 0}class="active"{/if} data-slide-to="{$k}"></li>
                {/foreach}
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                {foreach from=$slider key=k item=item}
                    <div class="item {if $k === 0}active{/if}" style='background-image: url("{$item.image}")'>
                        <div class="carousel-caption">
                            <a href="{$item.link}">
                                <h3>{$item.title}</h3>
                                <p>{$item.subtitle}</p>
                            </a>
                        </div>
                    </div>
                {/foreach}
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#header_carousel" role="button" data-slide="prev">

            </a>
            <a class="right carousel-control" href="#header_carousel" role="button" data-slide="next">

            </a>
        </div>
    </div>
</div>