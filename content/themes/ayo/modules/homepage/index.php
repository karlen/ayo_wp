<?php
namespace Modules;

use \Ayo\Core\Module;

class HomePage extends Module
{
    protected function setData(){
        global $lang;
        $data = get_field('content_'.$lang, 'option');
        $this->data =  ["content" => $data];
    }
}