<?php
namespace Modules;

use \Ayo\Core\Module;

class Footer extends Module
{
    protected function setData(){
        $this->data = array(
            "menu_items" => wp_get_nav_menu_items('Ayo')
        );
    }
}