<div data-module="footer">
    <footer class="page-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <h5 class="white-text">AYO Armenia</h5>
                    <p class="grey-text text-lighten-4">
                        AYO Armenia, Inc. has been servicing the local community for over 15 years by providing residential and commercial real estate services.
                    </p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12">
                    <h5 class="white-text">Links</h5>
                    <div class="links">
                        {foreach from=$menu_items item=menu_item}
                            <div><a class="grey-text text-lighten-3" href="{$menu_item->url}">{$menu_item->title}</a></div>
                        {/foreach}
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 footer_map" data-lat="59.4427" data-lng="24.7532" data-address="AYO Armenia, Yerevan">

                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © 2016 AYO Armenia, Inc.
                <a class="grey-text text-lighten-4 right" href="#!">AYO Armenia</a>
            </div>
        </div>
    </footer>
</div>