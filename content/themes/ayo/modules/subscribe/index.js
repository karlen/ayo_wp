import Validate from 'utils/validate'
import xhr from 'utils/xhr'

class Subscribe  {

    constructor(){
        this.attachEvents()
    }


    attachEvents() {
        $('[data-module="subscribe"] #submit').click(function(){
            var email = $('[data-module="subscribe"] #follow_email').val();
            var validate = new Validate();
            var is_email = validate.hooks.valid_email(email);
            if(!is_email){
                $('[data-module="subscribe"] #follow_email').addClass('error');
            }else {
                $('[data-module="subscribe"] #follow_email').removeClass('error');
                $('[data-module="subscribe"] .follow-submit #submit').val('........');
                xhr({
                    action: 'follow',
                    mail: email,
                    lang: window.ayo.lang
                },function(response){
                    $('[data-module="subscribe"] .follow-submit #submit').val('FOLLOW');
                    if(response === true){
                        $('[data-module="subscribe"] #follow_email').addClass('success');
                        $('[data-module="subscribe"] #follow_email').val('');
                        setTimeout(() => {
                            $('[data-module="subscribe"] #follow_email').removeClass('success');
                        }, 4500);
                    }else if(response === false){
                        $('[data-module="subscribe"] #follow_email').addClass('error');
                    }else {
                        $('[data-module="subscribe"] #follow_email').addClass('error');
                    }
                }, function(){
                    $('[data-module="subscribe"] #follow_email').addClass('error');
                    $('[data-module="subscribe"] .follow-submit #submit').val('FOLLOW');
                });
            }
        });
    }
}

new Subscribe();