<div data-module="subscribe">
    {php}
        global $lang;
        $texts = [
            "Follow" => [
                "en" => "Follow",
                "ru" => "Подписаться",
                "hy" => "Հետևել"
            ],
            "E-mail" => [
                "en" => "E-mail address",
                "ru" => "E-mail адрес",
                "hy" => "E-mail հասցե"
            ],
            "Follow-Newsletter" => [
                "en" => "Follow Our newsletter",
                "ru" => "Подписаться на наши новости",
                "hy" => "Հետևել մեր նորություններին"
            ]
        ];
    {/php}
    <div class="module_follow_form">
        <div class="container">
            <div class="row">
                <div class="title col-lg-8 col-md-10 col-sm-12  col-sm-offset-0  col-lg-offset-2 col-md-offset-1 ">
                    {php}echo $texts['Follow-Newsletter'][$lang];{/php}
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="follow_form col-lg-4 col-md-8 col-sm-12 col-sm-offset-0  col-lg-offset-4 col-md-offset-2">
                    <div class="col-sm-12">
                        <div class="form-group form-group-label">
                            <div class="row">
                                <div class="col-sm-12">
                                    <input class="input_email form-control" name="follow_email" id="follow_email" type="email" placeholder="{php}echo $texts['E-mail'][$lang];{/php}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group form-group-label">
                            <div class="row">
                                <div class="submit-button col-sm-12">
                                    <button class="btn btn-primary" id="submit" type="button" value="follow">{php}echo $texts['Follow'][$lang];{/php}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
