<div data-module="contact">
    {php}
        global $lang;
        $texts = [
        "Send" => [
        "en" => "Send",
        "ru" => " Отправить",
        "hy" => "Ուղարկել"
        ],
        "E-mail" => [
        "en" => "E-mail address",
        "ru" => "E-mail адрес",
        "hy" => "E-mail հասցե"
        ],
        "FullName" => [
        "en" => "Full Name",
        "ru" => "Полное имя",
        "hy" => "Անուն Ազգանուն"
        ],
        "YourMessage" => [
        "en" => "Your Message",
        "ru" => "Текст сообщения",
        "hy" => "Նամակ"
        ],
        "ContactUs" => [
        "en" => "Contact Us",
        "ru" => "Связь с нами",
        "hy" => "Կապ մեզ հետ"
        ]
        ];
    {/php}
    <div class="container">
        <h2>
            {php}echo $texts['ContactUs'][$lang];{/php}
        </h2>
        <div class="row">
            <div class="contact_form col-lg-12 col-md-12 col-sm-12">
                <form  id="messageForm" method="post" >
                    <div class="form-group form-group-label">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 input-field">
                                <label class="floating-label" for="userName">{php}echo $texts['FullName'][$lang];{/php}</label>
                                <input class="data form-control input_text" name="userName" id="userName" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-group-label">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 input-field">
                                <label class="floating-label" for="message_email">{php}echo $texts['E-mail'][$lang];{/php}</label>
                                <input class="data form-control input_email" name="message_email" id="message_email" type="email" >
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-group-label">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 input-field">
                                <label class="floating-label" for="message_text">{php}echo $texts['YourMessage'][$lang];{/php}</label>
                                <textarea class="data form-control input_textarea materialize-textarea textarea" name="message_text" id="message_text" ></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-group-label">
                        <button  class="step_submit btn btn-primary"  type="button" value="Send">{php}echo $texts['Send'][$lang];{/php}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>