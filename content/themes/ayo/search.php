<?php
/**
 * The template for displaying Search result pages.
 *
 * @package WordPress
 * @subpackage Ayo
 */

get_header();
load_modules([
    'Navigation',
    'Slider',
    'Search',
    'Subscribe',
    'Suggestions',
    'Contact',
    'Footer',
]);
get_footer();
